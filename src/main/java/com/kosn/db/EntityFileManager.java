package com.kosn.db;

import java.io.File;
import java.io.IOException;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.TypeFactory;
import com.kosn.entity.GameObject;
import com.kosn.entity.World;

public class EntityFileManager extends JsonManager {

    private static EntityFileManager instance = null;

    protected EntityFileManager() {
    }

    public static EntityFileManager getInstance() {
        if (instance == null) {
            instance = new EntityFileManager();
        }
        return instance;
    }

    public void makePlayerDir(String profileName) {
        File theSavesDir = new File(filePaths.get("savePath"));
        theSavesDir.mkdir();
        File thePlayerDir = new File(String.format(filePaths.get("profileSavePath"), profileName));
        thePlayerDir.mkdir();
    }

    private File createFileIfNeeded(String type, String objectName) {
        File theFile = new File(String.format(filePaths.get(type + "File"), objectName));
        try {
            theFile.createNewFile();
        } catch (IOException e) {
            System.out.println(String.format("failed to create file %s", theFile.toString()));
            e.printStackTrace();
        }
        return theFile;
    }

    private void writeToFile(File theFile, GameObject objectToWrite) {
        ObjectMapper mapper = new ObjectMapper();
        try {
            mapper.writeValue(theFile, objectToWrite);
        } catch (IOException e) {
            System.out.println(String.format("failed to write file %s", theFile.getName()));
            e.printStackTrace();
        }
    }

    public void writeWorld(World world) {
        makePlayerDir(world.getPlayer().getName());
        File theFile = createFileIfNeeded("world", world.getPlayer().getName());
        writeToFile(theFile, world);
    }

    public void deletePlayerDir(String profileName) {
        File thePlayerDir = new File(String.format(filePaths.get("profileSavePath"), profileName));
        thePlayerDir.delete();
    }

    public World loadWorld(String characterName) {
        this.file = new File(String.format(filePaths.get("worldFile"), characterName));
        ObjectMapper mapper = new ObjectMapper();
        World world = new World();
        try {
            world = mapper.readValue(this.file, TypeFactory.defaultInstance().constructType(World.class));
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return world;
    }
}
