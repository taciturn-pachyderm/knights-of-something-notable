package com.kosn.db;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

public abstract class JsonManager {

    protected File file;
    private static final String path = getPath();
    private static final String resourcesPath = String.format("%ssrc/main/resources/", path);
    private static final String entityRepositoryPath = "profiles/";
    protected static final String profilesPath = "game-saves";

    Map<String, String> filePaths = new HashMap<String, String>() {
        private static final long serialVersionUID = 1L;
        {
            put("savePath", "game-saves");
            put("profileSavePath", String.format("game-saves/%s", "%s"));
            put("playerFile", String.format("game-saves/%s/%s", "%s", "player.json"));
            put("roomsFile", String.format("game-saves/%s/%s", "%s", "rooms.json"));
            put("worldFile", String.format("game-saves/%s/%s", "%s", "world.json"));
            put("gameStateFile", String.format("game-saves/%s/%s", "%s", "state.json"));
        }
    };

    public static String getPath() {
        File currDir = new File(".");
        String path = "";
        path = currDir.getAbsolutePath();
        return path.substring(0, path.length() - 1);
    }
}
