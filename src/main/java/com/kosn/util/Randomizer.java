package com.kosn.util;

import java.util.Random;

public class Randomizer {

    public Integer getRandomBetweenZeroAndRange(int range) {
        Random r = new Random();
        return r.nextInt(range);
    }

    public Integer getRandomBetweenOneAndRange(int range) {
        Random r = new Random();
        return r.nextInt(range) + 1;
    }
}
