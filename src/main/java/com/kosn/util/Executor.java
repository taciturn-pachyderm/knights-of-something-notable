package com.kosn.util;

public class Executor {

    public void execute(Runnable runnable) {
        runnable.run();
    }

}
