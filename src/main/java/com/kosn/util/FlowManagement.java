package com.kosn.util;

public class FlowManagement {

    public static void wait(double seconds) {
        double millis = seconds * 1000;
        try {
            Thread.sleep((long) millis);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static void wait(int seconds) {
        wait((double) seconds);
    }

    public static void printSomeWaitingDots() {
        for (int i = 0; i < 5; i++) {
            wait(.25);
            System.out.print(".");
        }
        System.out.println("\n");
    }
}
