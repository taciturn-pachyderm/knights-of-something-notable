package com.kosn.util.printables;

public class CombatPrintables extends BasePrintables {

    private static final String CANNOT_ATTACK_THAT = "Attack what?";
    private static final String NOTHING_TO_ATTACK = "Nothing to attack.";
    public static final String INVENTORY_CHECK = "You rummage around in your inventory for %s.\n";
    public static final String EQUIPMENT_CHECK = "You examine your equipped %s.\n";
    public static final String ROOM_CHECK = "You glance around the room for %s.\n";

    // singleton
    private static CombatPrintables instance = null;

    protected CombatPrintables() {
    }

    public static CombatPrintables getInstance() {
        if (instance == null) {
            instance = new CombatPrintables();
        }
        return instance;
    }

    public void nonCombat() {
        print("You are not in combat.");
    }

    public void noCreatures() {
        print(CANNOT_ATTACK_THAT);
    }

    public void targetNotFound() {
        print(CANNOT_ATTACK_THAT);
    }

    public void nothingToAttack() {
        print(NOTHING_TO_ATTACK);
    }

    public void inCombat() {
        print("You're in combat!");
    }

    public void escapeStart() {
        print("Trying to escape...!");
    }

    public void escapeSuccess() {
        print("You have escaped!");
    }

    public void escapeWithMoneyLost(int calculateMoneyLost) {
        print("You have escaped but lost %s money.", calculateMoneyLost);
    }

    public void runAwayTaunt() {
        print("Running away are you?");
    }

    public void whichTarget(String target) {
        print("Target: %s\n", target);
    }

    public void cannotEscape() {
        print("You were unable to escape!");
    }

}
