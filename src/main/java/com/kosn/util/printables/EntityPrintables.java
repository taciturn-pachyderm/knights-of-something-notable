package com.kosn.util.printables;

import java.util.List;
import java.util.Map;

import com.kosn.application.Direction;
import com.kosn.entity.Character;
import com.kosn.entity.EquipSlot;
import com.kosn.entity.Item;
import com.kosn.entity.NonPlayer;
import com.kosn.entity.Player;
import com.kosn.entity.Room;
import com.kosn.util.TextHandler;

public class EntityPrintables extends BasePrintables {

    private static final String WELCOME = "\n\nWelcome to The Knights of Something Notable!\n";
    private static final String COMBAT_START = "Combat started...\n";
    private static final String COMBAT_END = "Combat ended\n";
    private static final String PRINT_ATTACK_ROUND = "%s hits %s for %s point(s) of damage!";
    private static final String NOT_IN_INVENTORY = "You don't have that in your inventory.";
    private static final String CANNOT_EQUIP = "You can't equip that.";
    private static final String UNEQUIP_ITEM = "You unequip the %s.";
    private static final String HP = "%s HP: %s/%s\n\n";
    private static final String EQUIP_ITEM = "You equip the %s.";
    private static final String EQUIPMENT_INFO = "%s: %s";
    private static final String ITEM_NOT_EQUIPPED = "You don't have a %s equipped.";
    private static final String BASE_ITEM_INFO = "\nYou behold the %s. It looks %s\n\n";
    private static final String DETAILED_ITEM_INFO = "With your amazing observational powers, you notice a few things about the %s:\n";
    private static final String EFFECT_INFO = "This item is consumable!\n%s Effect: %s %s\n";
    private static final String VICTORY = "\nYou have defeated the %s!";
    private static final String RECOVERY = "Your health has fully recovered!\n";
    private static final String SPOILS = "You have gained %d Exp. and %d money!\n";

    // singleton
    private static EntityPrintables instance = null;

    protected EntityPrintables() {
    }

    public static EntityPrintables getInstance() {
        if (instance == null) {
            instance = new EntityPrintables();
        }
        return instance;
    }

    public static void printWelcome() {
        print(WELCOME);
    }

    public static void printCombatStart() {
        print(COMBAT_START);
    }

    public static void printCombatEnd() {
        print(COMBAT_END);
    }

    public static void attackRound(String _playerName, String _nonPlayerName, int _playerAttack) {
        print(PRINT_ATTACK_ROUND, _playerName, _nonPlayerName, String.valueOf(_playerAttack));
    }

    public static void equipment(Map<String, Item> equipment) {
        print("Equipment:");
        for (Map.Entry<String, Item> entry : equipment.entrySet()) {
            print(EQUIPMENT_INFO, entry.getKey(), entry.getValue().getName());
        }
    }

    public static void equipment2(Map<EquipSlot, Item> equipment) {
        print("Equipment:");
        for (Map.Entry<EquipSlot, Item> entry : equipment.entrySet()) {
            print(EQUIPMENT_INFO, entry.getKey(), entry.getValue().getName());
        }
    }

    public static void inventory(List<Item> inventory) {
        print("Inventory:");
        for (Item item : inventory) {
            print(item.getName());
        }
    }

    public static void health(Character character) {
        print(HP, character.getName(), character.getHitPoints(), character.getMaxHitPoints());
    }

    public static void notInventoried() {
        print(NOT_IN_INVENTORY);

    }

    public static void unequipped(Item addBackIntoInventory) {
        print(UNEQUIP_ITEM, addBackIntoInventory);
    }

    public static void cannotEquip() {
        print(CANNOT_EQUIP);

    }

    public static void equipItem(String name) {
        print(EQUIP_ITEM, name);
    }

    public static void doNotHaveEquipped(String itemName) {
        print(ITEM_NOT_EQUIPPED, itemName);
    }

    public static void baseItemInfo(Item item) {
        print(BASE_ITEM_INFO, item.getName(), item.getDescription());

    }

    public static void detailedItemInfo(Item item) {
        print(DETAILED_ITEM_INFO, item.getName());
        TextHandler.printAsTwoColumnsLeftAligned("Slot", item.getSlot().toString());
        TextHandler.printAsTwoColumnsLeftAligned("Attack", String.valueOf(item.getAttack()));
        TextHandler.printAsTwoColumnsLeftAligned("Defense", String.valueOf(item.getDefense()));
    }

    public static void effectInfo(Item item) {
        print(EFFECT_INFO, item.getType(), String.valueOf(item.getEffectValue()), item.getEffectType().toString());

    }

    public static void victory(NonPlayer nonPlayer) {
        print(VICTORY, nonPlayer);
        print(SPOILS, nonPlayer.getExp(), nonPlayer.getMoney());
        print(RECOVERY);
    }

    public static void printHealth(NonPlayer nonPlayer) {
        print(HP, nonPlayer.getName(), String.valueOf(nonPlayer.getHitPoints()),
                String.valueOf(nonPlayer.getMaxHitPoints()));
    }

    public static void notHere() {
        print("You don't see that here.");

    }

    public static void doNotHave(String targetItemName) {
        print("You don't have that.");
    }

    public static void putDownItem(Item item) {
        print("You put down the %s.", item.getName());
    }

    public static void playerInfo(Player player) {
        TextHandler.printAsTwoColumnsLeftAligned("Name",
                String.format("%s (Lv. %d)", player.getName(), player.getLevel()));
        TextHandler.printAsTwoColumnsLeftAligned("HP",
                String.format("%d/%d", player.getHitPoints(), player.getMaxHitPoints()));
        TextHandler.printAsTwoColumnsLeftAligned("Attack", Integer.toString(player.getAttack()));
        TextHandler.printAsTwoColumnsLeftAligned("RangedAttack", Integer.toString(player.getRangedAttack()));
        TextHandler.printAsTwoColumnsLeftAligned("Defense", Integer.toString(player.getDefense()));
        TextHandler.printAsTwoColumnsLeftAligned("Exp",
                String.format("%d/%d", player.getExp(), player.getExpToNextLevel()));
        TextHandler.printAsTwoColumnsLeftAligned("Money", Integer.toString(player.getMoney()));
    }

    public static void unconcious(int moneyLost) {
        if (moneyLost > 0) {
            print("You have been knocked unconscious! %d money has been lost!\n", moneyLost);
            return;
        }
        print("You have been knocked unconscious!");
    }

    public static void pickUpItem(Item item) {
        print("You pick up the %s.\n", item.getName());
    }

    public static void levelUp(Player _player) {
        print("%s has attained level %d!\n", _player.getName(), _player.getLevel());
    }

    public static void roomExits(Room room) {
        print("\n", room.toString());
        print("Exits:");
        for (Map.Entry<Direction, String> entry : room.getExits().entrySet()) {
            print("%s: %s", entry.getKey(), entry.getValue());
        }
    }

    public static void roomItems(List<Item> items) {
        if (!items.isEmpty()) {
            print("\nRandom junk litters the floor: %s\n", items);
        }

    }

    public static void roomCreatures(List<NonPlayer> creatures) {
        if (!creatures.isEmpty()) {
            print("\nThere are creatures here:");
            print("%s", creatures);
        }

    }

    public static void printSpawnedCreature(List<NonPlayer> creatures) {
        print("A %s: has spawned!", creatures);

    }

    public static void roomTitle(Room room) {
        print(room.toString());
    }

    public static void connectingRooms() {
        print("Connecting the rooms");
    }

    public static void generatingRooms() {
        print("Generating rooms and loading them with junk");
    }

    public static void loadingEntities() {
        print("Loading things to do things with\n");
    }
}
