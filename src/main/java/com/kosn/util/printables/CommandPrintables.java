package com.kosn.util.printables;

import java.io.File;
import java.util.List;
import java.util.Map;

import com.kosn.application.Direction;
import com.kosn.entity.Examinable;

public class CommandPrintables extends BasePrintables {

    private static final String CANNOT_GO_THAT_WAY = "You can't go that way. Available exits:";
    private static final String ROOM_DOES_NOT_EXIST = "That target doesn't exist";
    private static final String CANNOT_ATTACK_THAT = "Attack what?";
    private static final String INVALID_COMMAND = "You can't do that. Type help to see available commands.";
    private static final String NOTHING_TO_ATTACK = "Nothing to attack.";
    public static final String INVENTORY_CHECK = "You rummage around in your inventory for %s.\n";
    public static final String EQUIPMENT_CHECK = "You examine your equipped %s.\n";
    public static final String ROOM_CHECK = "You glance around the room for %s.\n";

    // singleton
    private static CommandPrintables instance = null;

    protected CommandPrintables() {
    }

    public static CommandPrintables getInstance() {
        if (instance == null) {
            instance = new CommandPrintables();
        }
        return instance;
    }

    public void noCommand() {
        print(INVALID_COMMAND);
    }

    public void cannotGoThatWay(Map<Direction, String> exits) {
        print(CANNOT_GO_THAT_WAY);
        for (Direction direction : exits.keySet()) {
            print("%s: %s", direction, exits.get(direction));
        }
    }

    public void roomDoesNotExist() {
        print(ROOM_DOES_NOT_EXIST);
    }

    public void checkTarget(String phrase, Examinable target) {
        print(phrase, target.getName());
    }

    public void cannotUseThat() {
        print("You can't use that item.");
    }

    public void useItem(String name) {
        print("You use the %s!\n", name);
    }

    public void recoverHealth(int healthRecovered) {
        print("You have recovered %s health!\n", healthRecovered);
    }

    public void noEffect() {
        print("No effect");
    }

    public void alreadyFullHealth() {
        print("You're already at full health.");
    }

    public void whichTarget(String target) {
        print("Target: %s\n", target);
    }

    public void goodbye() {
        print("Goodbye!");
    }

    public void doNotHave(String target) {
        print("You don't have any %s", target);
    }

    public void command(String command) {
        print(command);
    }

    public void creatureEnteredRoom(String name, Direction direction) {
        print("%s has entered from the %s", name, direction);
    }

    public void creatureExitedRoom(String name, Direction direction) {
        print("%s has exited %s", name, direction);
    }

    public void creatureSpawned(String name) {
        print("%s spawned", name);
    }

    public void nameCharacter() {
        print("Name your character\n");
    }

    public void selectCharacter(File[] chars) {
        print("Select your character or provide a new name to start a new character\n");
        print("Current saves:\n");
        for (File save : chars) {
            print(save.getName());
        }
        print("\n");
    }

    public void loadCharacter(String selection) {
        print("\nLoading character %s", selection);
    }

    public void createCharacter(String selection) {
        print("\nCreating new character %s\n\n", selection);
    }

    public void commands(List<String> availableCommands) {
        for (String command : availableCommands) {
            command(command);
        }
    }
}
