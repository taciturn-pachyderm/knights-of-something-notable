package com.kosn.util.printables;

import java.util.List;

public class BasePrintables {

    public static void print(String toPrint, Object... args) {
        System.out.println(String.format(toPrint, args));
    }

    public <E> void print(List<E> toPrint) {
        System.out.println(toPrint);
    }

}