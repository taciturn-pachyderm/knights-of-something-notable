package com.kosn.entity;

import static com.google.common.collect.Lists.newArrayList;

import java.util.Collections;
import java.util.List;

import com.kosn.application.GameState;
import com.kosn.util.printables.EntityPrintables;

public class Player extends Character {

    private int expToNextLevel;
    GameState gameState = GameState.getInstance();

    public Player(PlayerDefaults pd) {
        this(pd.name, pd.description, pd.level, pd.money, pd.exp, pd.expToNextLevel, pd.hitPoints, pd.maxHitPoints);
    }

    public Player(String name, String description, int level, int money, int exp, int expToNextLevel, int hitPoints,
            int maxHitPoints) {
        super(name, description, level, money, exp, hitPoints, maxHitPoints);
        this.expToNextLevel = expToNextLevel;
    }

    public Player() {
    }

    public int getExpToNextLevel() {
        return expToNextLevel;
    }

    public void setExpToNextLevel(int expToNextLevel) {
        this.expToNextLevel = expToNextLevel;
    }

    public void levelUp() {
        this.level += 1;
        this.expToNextLevel += Math.round(expToNextLevel *= 1.2);
        this.maxHitPoints += Math.ceil(this.maxHitPoints * 0.05);
        this.hitPoints = this.maxHitPoints;
        this.updateStats();
        EntityPrintables.levelUp(this);
    }

    public String killPlayer() {
        Room defaultRoom = gameState.getWorld().getDefaultRoom();
        this.hitPoints = maxHitPoints;
        int moneyLost = 0;
        if (this.money > 0) {
            moneyLost = calculateMoneyLost(0.9);
            this.money = this.money - moneyLost;
        }
        EntityPrintables.unconcious(moneyLost);
        this.gameState.endCombat();
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        gameState.getWorld().setCurrentRoom(defaultRoom);
        defaultRoom.toString();
        return "respawn";
    }

    public void addToExp(int _exp) {
        this.exp += _exp;
    }

    public void adjustMoney(int _money) {
        this.money += _money;
    }

    public int calculateMoneyLost(double moneyLost) {
        return (int) Math.max(0, Math.round(this.money * moneyLost));
    }

    public void putItemInInventory(String targetItemName, Room thisRoom) {
        // Check if item is a valid room item
        Item item = checkRoomForItem(targetItemName, thisRoom);
        if (item == null) {
            EntityPrintables.notHere();
            return;
        }
        EntityPrintables.pickUpItem(item);
        inventory.add(item);
        Collections.sort(inventory);
        Room.removeItem(thisRoom, item);
    }

    private Item checkRoomForItem(String targetItemName, Room thisRoom) {
        Item item = null;
        List<Item> roomItems = newArrayList();
        roomItems = thisRoom.getItems();
        for (Item roomItem : roomItems) {
            if (roomItem.getName().equals(targetItemName) | roomItem.getName().startsWith(targetItemName)) {
                item = roomItem;
                break;
            }
        }
        return item;
    }

    public void removeItemFromInventory(String targetItemName, Room thisRoom) {
        Item item = checkInventoryForItem(targetItemName);
        if (item == null) {
            EntityPrintables.doNotHave(targetItemName);
            return;
        }

        EntityPrintables.putDownItem(item);
        inventory.remove(item);
        Room.addItem(thisRoom, item);
    }

    public void printStatus() {
        printInfo();
        printInventory();
        printEquipment();
    }

    public void printInfo() {
        EntityPrintables.playerInfo(this);
    }

    public void printInventory() {
        EntityPrintables.inventory(inventory);
    }

    public void printEquipment() {
        EntityPrintables.equipment2(equipment);
    }

    public Item checkInventoryForItem(String target) {
        for (Item checkItem : inventory) {
            if (checkItem.getName().equals(target)) {
                return checkItem;
            }
            if (checkItem.getName().startsWith(target)) {
                return checkItem;
            }
        }
        return null;
    }

    public Item checkEquipmentForItem(String target) {
        List<Item> equippedItems = newArrayList(equipment.values());
        for (Item checkItem : equippedItems) {
            if (checkItem.getName().equals(target)) {
                return checkItem;
            }
        }
        for (Item checkItem : equippedItems) {
            if (checkItem.getName().startsWith(target)) {
                return checkItem;
            }
        }

        return null;
    }

    @Override
    public String toString() {
        return "Name: " + this.getName() + "(Lv." + this.getLevel() + ")" + "\n" + "HP: " + this.getHitPoints() + "/"
                + this.getMaxHitPoints() + "\n" + "Attack: " + this.getAttack() + "\n" + "RangedAttack: " + this.getRangedAttack() + "\n" + "Defense: " + this.getDefense()
                + "\n" + "Exp: " + this.getExp() + "/" + this.expToNextLevel + "\n" + "Money: " + this.getMoney()
                + "\n";
    }
}
