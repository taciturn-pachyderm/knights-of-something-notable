package com.kosn.entity;
import static com.google.common.collect.Lists.newArrayList;

import java.util.ArrayList;
import java.util.Map;

import com.kosn.application.GameState;
import com.kosn.util.printables.EntityPrintables;

public class Equipment {

    ArrayList<Item> playerEquipment = newArrayList();

    // singleton
    private static Equipment instance = null;

    protected Equipment() {
    }

    public static Equipment getInstance() {
        if (instance == null) {
            instance = new Equipment();
        }
        return instance;
    }

    public void equipItem(String itemName) {
        Player player = GameState.getInstance().getWorld().getPlayer();
        Item item = player.checkInventoryForItem(itemName);
        if (item == null) {
            EntityPrintables.notInventoried();
            return;
        }
        if (item.getSlot() == null) {
            EntityPrintables.cannotEquip();
            return;
        }
        Item addBackIntoInventory = GameState.getInstance().getWorld().getPlayer().getEquipment().put(item.getSlot(), item);
        if (addBackIntoInventory != null) {
            player.getInventory().add(addBackIntoInventory);
            EntityPrintables.unequipped(addBackIntoInventory);
        }
        EntityPrintables.equipItem(item.getName());
        player.getInventory().remove(item);
        player.updateStats();
    }

    public void unequipItem(String itemName) {
        Player player = GameState.getInstance().getWorld().getPlayer();
        Item equippedItem = player.checkEquipmentForItem(itemName);
        if (equippedItem != null) {
            unequipItem(equippedItem.getSlot());
            return;
        }
        EntityPrintables.doNotHave(itemName);

    }

    public void unequipItem(EquipSlot slot) {
        Player player = GameState.getInstance().getWorld().getPlayer();
        Item item = player.getEquipment().get(slot);
        if (item != null) {
            EntityPrintables.unequipped(item);
            player.getEquipment().remove(item.getSlot());
            player.getInventory().add(item);
            player.setDefense(player.getDefense() - item.getDefense());
            player.setAttack(player.getAttack() - item.getAttack());
            return;
        }
    }

    public void print(Map<String, Item> equipment) {
        EntityPrintables.equipment(equipment);
    }
}
