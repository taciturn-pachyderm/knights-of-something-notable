package com.kosn.entity;

import static com.google.common.collect.Lists.newArrayList;
import static com.google.common.collect.Maps.newHashMap;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.kosn.util.printables.EntityPrintables;

public abstract class Character extends GameObject {

    protected int level;
    protected int money;
    protected int exp;
    protected int hitPoints;
    protected int maxHitPoints;
    protected int attack;
    protected int rangedAttack;
    protected int defense;
    protected ArrayList<Item> inventory = newArrayList();
    protected Map<EquipSlot, Item> equipment = newHashMap();
    protected ArrayList<Ability> deck = newArrayList();
    protected ArrayList<Ability> hand = newArrayList();

    public Character(String name, String description, int level, int money, int exp, int hitPoints, int maxHitPoints) {
        super(name, description);
        this.level = level;
        this.money = money;
        this.exp = exp;
        this.hitPoints = hitPoints;
        this.maxHitPoints = maxHitPoints;
    }

    public Character() {
    }

    public ArrayList<Item> getInventory() {
        return this.inventory;
    }

    public void setInventory(ArrayList<Item> inventory) {
        this.inventory = inventory;
    }

    public Map<EquipSlot, Item> getEquipment() {
        return this.equipment;
    }

    public void setEquipment(HashMap<EquipSlot, Item> equipment) {
        this.equipment = equipment;
    }

    public int getLevel() {
        return this.level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public int getHitPoints() {
        return this.hitPoints;
    }

    public void setHitPoints(int hitPoints) {
        this.hitPoints = hitPoints;
    }

    public int getMaxHitPoints() {
        return this.maxHitPoints;
    }

    public void setMaxHitPoints(int maxHitPoints) {
        this.maxHitPoints = maxHitPoints;
    }

    public int getAttack() {
        return this.attack;
    }

    public void setAttack(int attack) {
        this.attack = attack;
    }

    public int getRangedAttack() {
        return this.rangedAttack;
    }

    public void setRangedAttack(int rangedAttack) {
        this.rangedAttack = rangedAttack;
    }

    public int getDefense() {
        return this.defense;
    }

    public void setDefense(int defense) {
        this.defense = defense;
    }

    public int getExp() {
        return this.exp;
    }

    public void setExp(int exp) {
        this.exp = exp;
    }

    public int getMoney() {
        return money;
    }

    public void setMoney(int money) {
        this.money = money;
    }

    public void printEquipment(Map<String, Item> equipment) {
        EntityPrintables.equipment(equipment);
    }

    public void printInventory(List<Item> inventory) {
        EntityPrintables.inventory(inventory);
    }

    public void printHealth() {
        EntityPrintables.health(this);

    }

    public void updateStats() {
        int attackTotal = 0;
        int rangedAttackTotal = 0;
        int defenseTotal = 0;
        for (Item item : equipment.values()) {
            attackTotal += item.getAttack();
            rangedAttackTotal += item.getRangedAttack();
            defenseTotal += item.getDefense();
        }
        this.attack = attackTotal + this.level;
        this.rangedAttack = rangedAttackTotal + this.level;
        this.defense = defenseTotal + this.level;
    }

    @Override
    public String toString() {
        return this.name;
    }

    public void setInventory(String target, Room thisRoom) {
        // TODO Auto-generated method stub

    }
}
