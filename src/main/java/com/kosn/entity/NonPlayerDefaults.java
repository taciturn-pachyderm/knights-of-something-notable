package com.kosn.entity;

import com.kosn.application.GameState;

public final class NonPlayerDefaults {
    public String name, description;
    public int level, money, exp, hitPoints, maxHitPoints, attack, defense;
    GameState gameState = GameState.getInstance();

    private Player player = gameState.getWorld().getPlayer();
    private int playerLevel = player.getLevel();

    public NonPlayerDefaults(String name, String description) {
        this.name = name;
        this.description = description;
        this.level = this.playerLevel;
        this.money = 3;
        this.exp = player.getExpToNextLevel() - player.getExp();
        this.hitPoints = this.playerLevel * 2;
        this.maxHitPoints = this.playerLevel * 2;
        this.attack = this.playerLevel;
        this.defense = this.playerLevel;
    }
}
