package com.kosn.entity;

import static com.google.common.collect.Lists.newArrayList;
import static com.google.common.collect.Maps.newHashMap;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import com.kosn.application.Direction;
import com.kosn.util.printables.EntityPrintables;

public class Room implements Examinable {

    private String classType;
    private String name;
    private String description;
    private String type;
    private Boolean transition;
    private List<String> canConnectTo;
    private List<Item> items = newArrayList();
    private List<NonPlayer> creatures = newArrayList();
    private Map<Direction, String> exits = newHashMap();

    public Room() {
    }

    public Room(String name, String description, List<Item> items, List<NonPlayer> creatures,
            Map<Direction, String> exits) {
        this.name = name;
        this.description = description;
        this.items = items;
        this.creatures = creatures;
        this.exits = exits;
    }

    @Override
    public void printInfo() {
        EntityPrintables.roomTitle(this);
        EntityPrintables.roomExits(this);
        printItems();
        printCreatures();
    }

    @Override
    public String getName() {
        return this.name;
    }

    public void setItems(List<Item> roomItems) {
        this.items = roomItems;
    }

    public void setCreatures(List<NonPlayer> roomCreatures) {
        this.creatures = roomCreatures;
    }

    public Map<Direction, String> getExits() {
        return this.exits;
    }

    public List<Item> getItems() {
        return this.items;
    }

    public List<NonPlayer> getCreatures() {
        return this.creatures;
    }

    public void deleteItem(Item item) {
        this.items.remove(item);
    }

    public void addItem(Item item) {
        this.items.add(item);
        Collections.sort(this.items);
    }

    public void addCreature(NonPlayer creature) {
        this.creatures.add(creature);
    }

    public static void removeItem(Room room, Item item) {
        room.deleteItem(item);
    }

    public static void addItem(Room room, Item item) {
        room.addItem(item);
    }

    private void printItems() {
        EntityPrintables.roomItems(this.items);
    }

    public void printCreatures() {
        EntityPrintables.roomCreatures(this.creatures);
    }

    public void printSpawnedCreature() {
        EntityPrintables.printSpawnedCreature(this.creatures);

    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Boolean getTransition() {
        return transition;
    }

    public void setTransition(Boolean transition) {
        this.transition = transition;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setExits(Map<Direction, String> exits2) {
        this.exits = exits2;
    }

    public String getClassType() {
        return classType;
    }

    public void setClassType(String classType) {
        this.classType = classType;
    }

    public List<String> getCanConnectTo() {
        return canConnectTo;
    }

    public void setCanConnectTo(List<String> canConnectTo) {
        this.canConnectTo = canConnectTo;
    }

    @Override
    public String toString() {
        String toString = "<<<<<======= " + this.name + " =======>>>>>" + "\n" + this.description + "\n";
        return toString;
    }

    public NonPlayer checkRoomForNonPlayer(String target) {
        for (NonPlayer creature : this.creatures) {
            if (creature.getName().equals(target)) {
                return creature;
            }
            if (creature.getName().startsWith(target)) {
                return creature;
            }
        }
        return null;
    }

    public Item checkRoomForItem(String target) {
        for (Item checkItem : this.items) {
            if (checkItem.getName().equals(target)) {
                return checkItem;
            }
            if (checkItem.getName().startsWith(target)) {
                return checkItem;
            }
        }
        return null;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Room other = (Room) obj;
        if (name == null) {
            if (other.name != null)
                return false;
        } else if (!name.equals(other.name))
            return false;
        return true;
    }
}
