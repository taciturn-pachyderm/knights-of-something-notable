package com.kosn.entity;

import static com.google.common.collect.Maps.newHashMap;

import java.util.Map;

public class World extends GameObject {

    private Room currentRoom = null;
    private Room defaultRoom = null;
    private Player player;
    private Map<String, Room> rooms = newHashMap();

    public void changeRoom(Room nextRoom) {
        setCurrentRoom(nextRoom);
        nextRoom.printInfo();
    }

    public Room getCurrentRoom() {
        return currentRoom;
    }

    public void setCurrentRoom(Room currentRoom) {
        this.currentRoom = currentRoom;
    }

    public Room getDefaultRoom() {
        return defaultRoom;
    }

    public void setDefaultRoom(Room defaultRoom) {
        this.defaultRoom = defaultRoom;
    }

    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    public Map<String, Room> getRooms() {
        return rooms;
    }

    public void setRooms(Map<String, Room> rooms) {
        this.rooms = rooms;
    }
}
