package com.kosn.application;

import java.util.HashMap;
import java.util.Map;

import com.kosn.application.combat.Combat;
import com.kosn.entity.Player;
import com.kosn.entity.Room;
import com.kosn.util.Executor;
import com.kosn.util.Randomizer;
import com.kosn.util.printables.CombatPrintables;
import com.kosn.util.printables.CommandPrintables;

public class PlayerLocationHandler {

    CommandPrintables commandPrintables = CommandPrintables.getInstance();
    CombatPrintables combatPrintables = CombatPrintables.getInstance();
    Combat combat = Combat.getInstance();
    String escapeStatus = "";
    Executor executor = new Executor();
    Randomizer randomizer = new Randomizer();

    // singleton
    private static PlayerLocationHandler instance = null;

    protected PlayerLocationHandler() {
    }

    public static PlayerLocationHandler getInstance() {
        if (instance == null) {
            instance = new PlayerLocationHandler();
        }
        return instance;
    }

    Map<Integer, Runnable> rollResults = new HashMap<Integer, Runnable>() {
        private static final long serialVersionUID = 1L;
        {
            put(0, () -> unableToEscape());
            put(1, () -> escapeWithDamage());
            put(2, () -> escapeWithDamage());
            put(3, () -> escapeWithMoneyLoss());
            put(4, () -> escapeWithMoneyLoss());
            put(5, () -> escape());
        }
    };

    public void verifyRoomChange(String direction) {
        Room currentRoom = GameState.getInstance().getWorld().getCurrentRoom();
        Room nextRoom = getNextRoom(direction);
        if (nextRoom == null) {
            this.commandPrintables.cannotGoThatWay(currentRoom.getExits());
            return;
        }
        changeRooms(nextRoom);
    }

    Room getNextRoom(String direction) {
        Direction enumDirection;
        try {
            enumDirection = Direction.valueOf(direction);
        } catch (IllegalArgumentException e) {
            return new Room();
        }
        return GameState.getInstance().getWorld().getRooms().get(getCurrentRoom().getExits().get(enumDirection));
    }

    private Room getCurrentRoom() {
        return GameState.getInstance().getWorld().getCurrentRoom();
    }

    void changeRooms(Room nextRoom) {
        if (GameState.getInstance().getCombat()) {
            this.combatPrintables.inCombat();
            executeEscapeRoll();
            if (this.escapeStatus.equals("failure")) {
                return;
            }
            GameState.getInstance().endCombat();
        }
        executeRoomChange(nextRoom);
    }

    void executeRoomChange(Room nextRoom) {
        GameState.getInstance().getWorld().changeRoom(nextRoom);
    }

    void executeEscapeRoll() {
        this.combatPrintables.escapeStart();
        executeEscapeCondition(getEscapeCondition());
    }

    private Runnable getEscapeCondition() {
        return this.rollResults.get(randomizer.getRandomBetweenZeroAndRange(rollResults.size()));
    }

    private void executeEscapeCondition(Runnable escapeMethod) {
        executor.execute(escapeMethod);
    }

    void unableToEscape() {
        this.combatPrintables.cannotEscape();
        processNonPlayerAttackRound();
        this.escapeStatus = "failure";
    }

    void escapeWithMoneyLoss() {
        updatePlayerMoney();
        this.escapeStatus = "success";
    }

    private void updatePlayerMoney() {
        Player player = GameState.getInstance().getWorld().getPlayer();
        int moneyLost = player.calculateMoneyLost(0.05);
        this.combatPrintables.escapeWithMoneyLost(moneyLost);
        player.adjustMoney(-moneyLost);
    }

    void escapeWithDamage() {
        if (processNonPlayerAttackRound().equals("respawned")) {
            return;
        }
        escape();
    }

    void escape() {
        this.combatPrintables.runAwayTaunt();
        this.escapeStatus = "success";
    }

    String processNonPlayerAttackRound() {
        GameState gameState = GameState.getInstance();
        String combatResult = this.combat.processNonPlayerAttack(gameState.getWorld().getPlayer(), gameState.getCurrentCombatTarget());
        return combatResult;
    }
}
