package com.kosn.application;

import static com.google.common.collect.Lists.newArrayList;
import static com.google.common.collect.Maps.newHashMap;

import java.io.File;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Scanner;

import com.kosn.db.EntityFactory;
import com.kosn.db.EntityFileManager;
import com.kosn.entity.Item;
import com.kosn.entity.ItemType;
import com.kosn.entity.NonPlayer;
import com.kosn.entity.Player;
import com.kosn.entity.PlayerDefaults;
import com.kosn.entity.Room;
import com.kosn.entity.World;
import com.kosn.util.Randomizer;
import com.kosn.util.printables.CommandPrintables;
import com.kosn.util.printables.EntityPrintables;

public class WorldBuilder {

    private List<NonPlayer> creaturePool = newArrayList();
    private List<Room> roomPool = newArrayList();
    private List<Item> itemPool = newArrayList();
    private Map<String, Room> rooms = newHashMap();
    private static final int exitsUpperBound = 3;

    private List<Item> itemsAddedAtLoad = newArrayList();

    private final Direction[] directions = Direction.values();
    private final int directionSize = directions.length;
    private String characterName;
    private EntityFactory entityFactory = EntityFactory.getInstance();
    private EntityFileManager entityFileManager = EntityFileManager.getInstance();
    private CommandPrintables utilPrintables = CommandPrintables.getInstance();
    private Randomizer randomizer = new Randomizer();

    // singleton
    private static WorldBuilder instance = null;

    protected WorldBuilder() {
    }

    public static WorldBuilder getInstance() {
        if (instance == null) {
            instance = new WorldBuilder();
        }
        return instance;
    }

    public void buildWorld() {
        loadEntityPools();
        File[] saves = loadSaveProfiles();
        String selection = characterSelectionPrompt(saves);
        if (characterExists(saves, selection)) {
            this.utilPrintables.loadCharacter(selection);
            loadCharacter(selection);
            return;
        }
        GameState.getInstance().setWorld(buildNewWorld(selection));
    }

    private File[] loadSaveProfiles() {
        File[] profiles = new File("game-saves").listFiles(File::isDirectory);
        if (profiles == null) {
            return new File[0];
        }
        return profiles;
    }

    public String characterSelectionPrompt(File[] saves) {
        if (saves.length > 0) {
            this.utilPrintables.selectCharacter(saves);
        } else {
            this.utilPrintables.nameCharacter();
        }
        @SuppressWarnings("resource")
        Scanner characterSelection = new Scanner(System.in);
        return characterSelection.nextLine();
    }

    private boolean characterExists(File[] saves, String selection) {
        if (saves != null)
            for (File g : saves) {
                if (g.getName().equals(selection)) {
                    return true;
                }
            }
        return false;
    }

    public World buildNewWorld(String selection) {
        this.utilPrintables.createCharacter(selection);
        Player player = new Player(new PlayerDefaults());
        player.updateStats();
        player.setName(selection);
        World world = new World();
        world.setPlayer(player);

        generateNewRooms();
        connectTheNewRooms();

        world.setRooms(rooms);
        world.setCurrentRoom(getRandomRoom());
        world.setDefaultRoom(getRandomRoom());
        return world;
    }

    /**
     * add 0-2 items if a nonconsumable has been added to a room already, don't
     * add it again use itemsAdded map to check this consumable items - doesn't
     * matter how many get added
     */
    private Room loadRoomWithJunk(Room r) {
        Room roomToJunkify = r;
        int randomIndex = 0;
        Item itemToAdd = null;
        List<Item> roomItems = newArrayList();
        int numItemsToAdd = randomizer.getRandomBetweenOneAndRange(3);

        for (int i = 0; i < numItemsToAdd; i++) {
            randomIndex = randomizer.getRandomBetweenZeroAndRange(itemPool.size());
            itemToAdd = itemPool.get(randomIndex);

            if (itemToAdd.getType().equals(ItemType.nonconsumable)) {
                if (itemsAddedAtLoad.contains(itemToAdd)) {
                    continue;
                }
            }
            itemsAddedAtLoad.add(itemToAdd);
            roomItems.add(itemToAdd);
        }
        roomToJunkify.setItems(roomItems);
        return roomToJunkify;
    }

    private void connectTheNewRooms() {
        Map<Direction, String> currentExits = newHashMap();
        Room roomToAdd = null;
        Direction directionToAdd = null;

        addReciprocalExitsForPredefinedConnections();

        // generate additional exits
        for (Room r : rooms.values()) {
            currentExits = r.getExits();
            int numExitsToAdd = getNumberOfExitsToAdd(r);
            Map<Direction, String> exitsOfRoomToAdd = newHashMap();

            while (currentExits.size() < numExitsToAdd) {
                directionToAdd = getDirectionToAdd();
                // don't add the same direction to the same room twice
                if (roomAlreadyHasExit(currentExits, directionToAdd)) {
                    continue;
                }
                roomToAdd = getRandomRoom();
                exitsOfRoomToAdd = roomToAdd.getExits();
                // don't add the exit if exit is to the current room
                if (selectedRoomIsCurrentRoom(r, roomToAdd)) {
                    continue;
                }
                // don't add the exit if an exit to the target room already exits to the current room
                if (roomAlreadyExitsToTargetRoom(currentExits, roomToAdd)) {
                    // require 1 fewer exit if this condition is met
                    if (numExitsToAdd > 1) {
                        numExitsToAdd -= 1;
                    }
                    continue;
                }
                // don't add exit if target room has opposite direction set
                if (targetRoomAlreadyHasSelectedExit(exitsOfRoomToAdd, directionToAdd)) {
                    continue;
                }
                // don't add the exit if the room is not of the same type or not a transition room
                if (roomTypesAreIncompatible(r, roomToAdd)) {
                    continue;
                }
                // TODO: add check to ensure all rooms are accessible
                // add the exit
                r.getExits().put(directionToAdd, roomToAdd.getName());
                // add the opposite exit to the target room
                rooms.get(roomToAdd.getName()).getExits().put(Direction.getOppositeDirection(directionToAdd), r.getName());
            }
        }
        EntityPrintables.connectingRooms();
    }

    private int getNumberOfExitsToAdd(Room room) {
        int eligibleRoomsToMoveTo = countEligibleRoomsToMoveTo(room);
        int numMaxExits = determineMaxExits(eligibleRoomsToMoveTo);
        return randomizer.getRandomBetweenOneAndRange(numMaxExits);
    }

    private Direction getDirectionToAdd() {
        return directions[randomizer.getRandomBetweenZeroAndRange(directionSize)];
    }

    private boolean roomAlreadyHasExit(Map<Direction, String> currentExits, Direction directionToAdd) {
        if (currentExits.containsKey(directionToAdd)) {
            return true;
        }
        return false;
    }

    private boolean selectedRoomIsCurrentRoom(Room currentRoom, Room roomToAdd) {
        if (currentRoom.equals(roomToAdd)) {
            return true;
        }
        return false;
    }

    private boolean roomAlreadyExitsToTargetRoom(Map<Direction, String> currentExits, Room roomToAdd) {
        if (currentExits.containsValue(roomToAdd.getName())) {
            return true;
        }
        return false;
    }

    private boolean targetRoomAlreadyHasSelectedExit(Map<Direction, String> exitsOfRoomToAdd, Direction directionToAdd) {
        if (!exitsOfRoomToAdd.keySet().isEmpty()) {
            if (exitsOfRoomToAdd.containsKey(Direction.getOppositeDirection(directionToAdd))) {
                return true;
            }
        }
        return false;
    }

    private boolean roomTypesAreIncompatible(Room fromRoom, Room toRoom) {
        // if the types are different, check transition
        if (!fromRoom.getType().equals(toRoom.getType())) {
            // check whether they're both transition rooms
            if (toRoom.getTransition() && fromRoom.getTransition()) {
                // check if room to add is in list of eligible transition rooms
                if (!fromRoom.getCanConnectTo().contains(toRoom.getName())) {
                    return true;
                }
            } else {
                return true;
            }
        }
        return false;
    }

    /**
     * generate opposite direction connections for rooms with pre-defined exits
     */
    private void addReciprocalExitsForPredefinedConnections() {
        for (Room r : rooms.values()) {
            for (Direction exit : r.getExits().keySet()) {
                rooms.get(r.getExits().get(exit)).getExits().put(Direction.getOppositeDirection(exit), r.getName());
            }
        }
    }

    private int determineMaxExits(int numRoomsOfType) {
        return Math.min(numRoomsOfType, exitsUpperBound);
    }

    private int countEligibleRoomsToMoveTo(Room room) {
        int numMatchingRooms = 0;
        for (Room r : rooms.values()) {
            if (r.getType().equals(room.getType())) {
                numMatchingRooms++;
                continue;
            }
            if (room.getTransition() && r.getTransition()) {
                numMatchingRooms++;
            }
        }
        // subtract 1 to exclude current room from count
        numMatchingRooms--;
        return numMatchingRooms;
    }

    private void generateNewRooms() {
        for (Room r : roomPool) {
            loadRoomWithJunk(r);
            rooms.put(r.getName(), r);
        }
        EntityPrintables.generatingRooms();
    }

    public void loadEntityPools() {
        creaturePool = entityFactory.createNonPlayers();
        roomPool = entityFactory.createRooms();
        itemPool = entityFactory.createItems();
        EntityPrintables.loadingEntities();
    }

    public List<NonPlayer> getCreaturePool() {
        return creaturePool;
    }

    public List<Item> getItemPool() {
        return itemPool;
    }

    public Room getRandomRoom() {
        final Random random = new Random();
        List<Room> roomsArray = newArrayList(rooms.values());
        int randomIndex = random.nextInt(rooms.size());
        return roomsArray.get(randomIndex);
    }

    public Map<String, Room> getRooms() {
        return rooms;
    }

    public void setRooms(Map<String, Room> rooms2) {
        this.rooms = rooms2;
    }

    public void loadCharacter(String selection) {
        this.characterName = selection;
        GameState.getInstance().setWorld(entityFileManager.loadWorld(this.characterName));
    }
}
