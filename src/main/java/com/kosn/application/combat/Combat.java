package com.kosn.application.combat;

import java.util.List;

import com.kosn.application.GameState;
import com.kosn.entity.NonPlayer;
import com.kosn.entity.Player;
import com.kosn.entity.Room;
import com.kosn.util.printables.EntityPrintables;

public class Combat {

    private static Room combatRoom;
    private static Player combatPlayer;
    private static NonPlayer combatNonPlayer;
    GameState gameState = GameState.getInstance();

    private static Combat instance = null;

    protected Combat() {
    }

    public static Combat getInstance() {
        if (instance == null) {
            instance = new Combat();
        }
        return instance;
    }

    public void attackNonPlayer(NonPlayer creature, Room room, Player player) {
        combatRoom = room;
        combatPlayer = player;
        combatNonPlayer = creature;
        this.gameState.toggleCombatOn(combatNonPlayer);
        if (processPlayerAttack().equals("continue")) {
            processNonPlayerAttack(player, combatNonPlayer);
        }
    }

    private String processPlayerAttack() {
        int damage = calculateDamage(combatNonPlayer.getDefense(), combatPlayer.getAttack());
        combatNonPlayer.lowerHitPoints(damage);
        EntityPrintables.attackRound(combatPlayer.getName(), combatNonPlayer.getName(), damage);
        if (combatNonPlayer.getHitPoints() <= 0) {
            killNonPlayer(combatPlayer, combatNonPlayer);
            this.gameState.endCombat();
            if (combatPlayer.getExp() >= combatPlayer.getExpToNextLevel()) {
                combatPlayer.levelUp();
            }
            removeCreatureFromRoom();
            return "end round";
        }
        combatNonPlayer.printHealth();
        return "continue";
    }

    private void removeCreatureFromRoom() {
        List<NonPlayer> roomCreatures = combatRoom.getCreatures();
        int index = 0;

        for (NonPlayer np : roomCreatures) {
            if (np.equals(combatNonPlayer)) {
                roomCreatures.remove(index);
                combatRoom.setCreatures(roomCreatures);
                break;
            }
        }
    }

    public String processNonPlayerAttack(Player player, NonPlayer nonPlayer) {
        int damage = calculateDamage(player.getDefense(), nonPlayer.getAttack());
        player.setHitPoints(player.getHitPoints() - damage);
        EntityPrintables.attackRound(nonPlayer.getName(), "you", damage);
        player.printHealth();
        if (player.getHitPoints() <= 0) {
            player.killPlayer();
            return "respawned";
        }
        return "";
    }

    private int calculateDamage(int targetDefense, int sourceAttack) {
        return Math.max(1, sourceAttack - targetDefense);
    }

    public void killNonPlayer(Player player, NonPlayer nonPlayer) {
        player.addToExp(nonPlayer.getExp());
        player.adjustMoney(nonPlayer.getMoney());
        player.setHitPoints(player.getMaxHitPoints());
        EntityPrintables.victory(nonPlayer);
        gameState.getWorld().getCurrentRoom().getCreatures().remove(nonPlayer);
    }
}
