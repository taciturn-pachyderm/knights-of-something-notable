package com.kosn.application;

import com.kosn.application.command.CommandLoader;
import com.kosn.application.creature.CreatureManager;
import com.kosn.util.printables.EntityPrintables;

public class Application {

    private static CommandLoader commandLoader = CommandLoader.getInstance();
    private static Thread creatureManagerThread;
    private static WorldBuilder worldBuilder = WorldBuilder.getInstance();

    // static Input input = Input.getInstance();

    public static void main(String args[]) {
        setUpGame();

        // Start game
        while (GameState.getInstance().getPlaying()) {
            commandLoader.processCommand(Input.getCommand());
            commandLoader.processCommand("save");
        }
        System.exit(0);
    }

    private static void setUpGame() {
        EntityPrintables.printWelcome();
        worldBuilder.buildWorld();
        GameState.getInstance().getWorld().getCurrentRoom().printInfo();

        // start creature manager
        creatureManagerThread = new Thread(new CreatureManager());
        creatureManagerThread.start();

        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
            System.exit(0);
        }
    }
}
