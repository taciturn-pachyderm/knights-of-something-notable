package com.kosn.application;

import com.kosn.entity.NonPlayer;
import com.kosn.entity.World;
import com.kosn.util.printables.EntityPrintables;

public class GameState {

    private boolean playing = true;
    private NonPlayer currentCombatTarget;
    private boolean combat = false;
    public World world;

    private static GameState instance = null;

    protected GameState() {
    }

    public static GameState getInstance() {
        if (instance == null) {
            instance = new GameState();
        }
        return instance;
    }

    public boolean getPlaying() {
        return playing;
    }

    public void setPlaying(boolean playing) {
        this.playing = playing;
    }

    public NonPlayer getCurrentCombatTarget() {
        return currentCombatTarget;
    }

    public void setCurrentCombatTarget(NonPlayer nonPlayer) {
        currentCombatTarget = nonPlayer;
    }

    public boolean getCombat() {
        return combat;
    }

    public void setCombat(boolean _combat) {
        combat = _combat;
    }

    public void toggleCombatOn(NonPlayer nonPlayer) {
        if (!getCombat()) {
            EntityPrintables.printCombatStart();
            setCombat(true);
            setCurrentCombatTarget(nonPlayer);
        }
    }

    public void endCombat() {
        setCombat(false);
        setCurrentCombatTarget(null);
        EntityPrintables.printCombatEnd();
    }

    public World getWorld() {
        return world;
    }

    public void setWorld(World world) {
        this.world = world;
    }

}
