package com.kosn.application.creature;

import static com.google.common.collect.Lists.newArrayList;
import static com.google.common.collect.Maps.newHashMap;

import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;

import com.kosn.application.Direction;
import com.kosn.application.GameState;
import com.kosn.application.WorldBuilder;
import com.kosn.entity.NonPlayer;
import com.kosn.entity.NonPlayerDefaults;
import com.kosn.entity.Room;
import com.kosn.util.FlowManagement;
import com.kosn.util.printables.CommandPrintables;

public class CreatureManager implements Runnable {

    private int creatureCount = 0;
    private final int maxCreatureCount = 20;
    private static WorldBuilder worldBuilder = WorldBuilder.getInstance();
    private GameState gameState = GameState.getInstance();
    private List<Room> roomsWithCreatures = newArrayList();
    private final Random random = new Random();
    private final double timeToWait = 10;
    CommandPrintables commandPrintables = CommandPrintables.getInstance();

    @Override
    public void run() {
        while (gameState.getPlaying()) {
            processCreatures();
            FlowManagement.wait(timeToWait);
        }
    }

    private void processCreatures() {
        // don't spawn new creatures if in combat
        if (gameState.getCombat()) {
            return;
        }
        roomsWithCreatures = getRoomsWithCreatures();
        if (roomsWithCreatures.isEmpty() || creatureCount < maxCreatureCount) {
            spawnCreature();
        }
        // return after first creature spawn; don't want to move it right away
        if (creatureCount <= 1) {
            return;
        }
        Iterator<Room> roomIterator = roomsWithCreatures.iterator();
        while (roomIterator.hasNext()) {
            Room room = roomIterator.next();
            Iterator<NonPlayer> creatureIterator = room.getCreatures().iterator();
            while (creatureIterator.hasNext()) {
                if (creatureIterator.next().getLevel() < gameState.getWorld().getPlayer().getLevel() - 3) {
                    creatureIterator.remove();
                    continue;
                }
                if (shouldCreatureMove()) {
                    Map<Direction, String> roomToAddCreatureTo = getRoomToMoveCreatureTo(room);
                    moveCreatures(roomToAddCreatureTo, room, creatureIterator);
                }
            }
        }
    }

    private boolean shouldCreatureMove() {
        int rollForMove = random.nextInt(100);
        if (rollForMove > 30) {
            return false;
        }
        return true;
    }

    private List<Room> getRoomsWithCreatures() {
        this.creatureCount = 0;
        List<Room> roomsWithCreatures = newArrayList();
        for (Room room : gameState.getWorld().getRooms().values()) {
            if (!room.getCreatures().isEmpty()) {
                roomsWithCreatures.add(room);
                this.creatureCount += room.getCreatures().size();
            }
        }
        return roomsWithCreatures;
    }

    private void spawnCreature() {
        if (gameState.getCombat()) {
            return;
        }
        Room roomToAddCreatureTo = gameState.getWorld().getRooms().get(getRoomToAddCreatureTo());
        NonPlayer creatureToAdd = getCreatureToAdd();
        addCreatureToRoom(roomToAddCreatureTo, creatureToAdd);
    }

    public void removeCreatureFromRoom(Room room, NonPlayer creature) {
        gameState.getWorld().getRooms().get(room.getName()).getCreatures().remove(creature);
    }

    private void addCreatureToRoom(Room roomToAddCreatureTo, NonPlayer creatureToAdd) {
        gameState.getWorld().getRooms().get(roomToAddCreatureTo.getName()).addCreature(creatureToAdd);
        if (gameState.getWorld().getCurrentRoom().equals(roomToAddCreatureTo)) {
            this.commandPrintables.creatureSpawned(creatureToAdd.getName());
        }
        this.creatureCount++;
    }

    private NonPlayer getCreatureToAdd() {
        NonPlayer creature = getRandomCreature();
        return new NonPlayer(new NonPlayerDefaults(creature.getName(), creature.getDescription()));
    }

    private NonPlayer getRandomCreature() {
        List<NonPlayer> creaturePool = worldBuilder.getCreaturePool();
        int randomIndex = new Random().nextInt(creaturePool.size());
        return creaturePool.get(randomIndex);
    }

    private String getRoomToAddCreatureTo() {
        Room currentRoom = gameState.getWorld().getCurrentRoom();
        List<String> availableRooms = newArrayList(currentRoom.getExits().values());
        availableRooms.add(currentRoom.getName());
        int randomIndex = random.nextInt(availableRooms.size());
        return availableRooms.get(randomIndex);
    }

    private Map<Direction, String> getRoomToMoveCreatureTo(Room room) {
        Map<Direction, String> selectedRoom = newHashMap();
        selectedRoom.putAll(room.getExits());
        List<Direction> availableDirections = newArrayList(selectedRoom.keySet());
        int randomIndex = random.nextInt(availableDirections.size());
        Direction direction = availableDirections.get(randomIndex);
        // remove all other directions except the selected direction
        selectedRoom.entrySet().removeIf(e -> !e.getKey().equals(direction));
        return selectedRoom;
    }

    private void moveCreatures(Map<Direction, String> singleRoomMap, Room roomMovingFrom,
            Iterator<NonPlayer> creatureIterator) {
        if (!creatureIterator.hasNext()) {
            return;
        }
        GameState gameState = GameState.getInstance();
        NonPlayer creature = creatureIterator.next();
        Direction direction = newArrayList(singleRoomMap.keySet()).get(0);
        Room roomMovingTo = gameState.getWorld().getRooms().get(singleRoomMap.get(direction));
        gameState.getWorld().getRooms().get(roomMovingTo.getName()).addCreature(creature);
        creatureIterator.remove();
        if (gameState.getWorld().getCurrentRoom().equals(roomMovingTo)) {
            this.commandPrintables.creatureEnteredRoom(creature.getName(), Direction.getOppositeDirection(direction));
        }
        if (gameState.getWorld().getCurrentRoom().equals(roomMovingFrom)) {
            this.commandPrintables.creatureExitedRoom(creature.getName(), direction);
        }
    }
}
