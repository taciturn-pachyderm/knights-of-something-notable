package com.kosn.application;

import java.util.Scanner;

public class Input {

    public static String target = "";

    public static String getCommand() {

        Scanner in = new Scanner(System.in);
        System.out.println(">");
        String input = in.nextLine().toLowerCase();
        String command = "";

        if (input.contains(" ")) {
            command = input.substring(0, input.indexOf(" "));
            target = input.substring(input.indexOf(" ") + 1).toLowerCase();
        } else {
            command = input;
            target = "";
        }
        return command;
    }

    public static String getTarget() {
        return target;
    }

    public static void setTarget(String previousTarget) {
        target = previousTarget;
    }
}
