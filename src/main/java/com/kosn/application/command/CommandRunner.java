package com.kosn.application.command;

import com.kosn.util.printables.CommandPrintables;

public class CommandRunner {

    CommandPrintables commandPrintables = CommandPrintables.getInstance();

    // singleton
    private static CommandRunner instance = null;

    protected CommandRunner() {
    }

    public static CommandRunner getInstance() {
        if (instance == null) {
            instance = new CommandRunner();
        }
        return instance;
    }

    void runTheThing(Runnable thingToRun) {
        if (thingToRun != null) {
            thingToRun.run();
        } else {
            this.commandPrintables.noCommand();
        }
    }
}
