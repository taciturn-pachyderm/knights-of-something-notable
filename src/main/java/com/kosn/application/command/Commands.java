package com.kosn.application.command;

import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.text.WordUtils;

import com.kosn.application.GameState;
import com.kosn.application.Input;
import com.kosn.application.PlayerLocationHandler;
import com.kosn.application.combat.Combat;
import com.kosn.db.EntityFileManager;
import com.kosn.entity.Equipment;
import com.kosn.entity.Item;
import com.kosn.entity.ItemType;
import com.kosn.entity.NonPlayer;
import com.kosn.entity.Player;
import com.kosn.entity.Room;
import com.kosn.util.printables.CombatPrintables;
import com.kosn.util.printables.CommandPrintables;
import com.kosn.util.printables.EntityPrintables;

public class Commands {

    private Room nextRoom;
    private GameState gameState = GameState.getInstance();
    CommandHelper commandHelper = CommandHelper.getInstance();

    private static final String INVENTORY = "inventory";
    CommandPrintables commandPrintables = CommandPrintables.getInstance();
    CombatPrintables combatPrintables = CombatPrintables.getInstance();
    CommandRunner commandRunner = CommandRunner.getInstance();
    PlayerLocationHandler playerLocationHandler = PlayerLocationHandler.getInstance();
    Combat combat = Combat.getInstance();
    EntityFileManager entityExporter = EntityFileManager.getInstance();
    Equipment equipment = Equipment.getInstance();

    // singleton
    private static Commands instance = null;

    protected Commands() {
    }

    public static Commands getInstance() {
        if (instance == null) {
            instance = new Commands();
        }
        return instance;
    }

    private Player getThePlayer() {
        return gameState.getWorld().getPlayer();
    }

    private List<NonPlayer> getTheCreatures() {
        return getTheRoom().getCreatures();
    }

    private String getTheTarget() {
        return Input.getTarget();
    }

    private Room getTheRoom() {
        return gameState.getWorld().getCurrentRoom();
    }

    protected void useItem() {
        String target = getTheTarget();
        Player player = getThePlayer();
        Map<String, Item> availableItems = this.commandHelper.getUsableItems(getTheRoom(), target);
        Item itemToConsume = availableItems.get(INVENTORY);

        if (itemToConsume == null) {
            this.commandPrintables.doNotHave(target);
            return;
        }

        if (!itemToConsume.getType().equals(ItemType.consumable)) {
            this.commandPrintables.cannotUseThat();
            return;
        }

        switch (itemToConsume.getEffectType()) {
        case health:
            int currentHitPoints = player.getHitPoints();
            int maxHitPoints = player.getMaxHitPoints();
            if (currentHitPoints < maxHitPoints) {
                int healthAfterRecover = currentHitPoints + itemToConsume.getEffectValue();
                int finalHealth = healthAfterRecover <= maxHitPoints ? healthAfterRecover : maxHitPoints;
                player.setHitPoints(finalHealth);
                player.getInventory().remove(itemToConsume);
                this.commandPrintables.useItem(itemToConsume.getName());
                this.commandPrintables.recoverHealth(finalHealth - currentHitPoints);
                break;
            }
            this.commandPrintables.alreadyFullHealth();
            break;
        case attack:
            player.setAttack(player.getAttack() + itemToConsume.getEffectValue());
            break;
        case defense:
            player.setDefense(player.getDefense() + itemToConsume.getEffectValue());
            break;
        case other:
        default:
            this.commandPrintables.noEffect();
            break;
        }
    }

    protected void warp() {
        String target = WordUtils.capitalize(getTheTarget());

        this.nextRoom = gameState.getWorld().getRooms().get(target);

        if (this.nextRoom == null) {
            this.commandPrintables.roomDoesNotExist();
            EntityPrintables.roomExits(getTheRoom());
            return;
        }
        gameState.getWorld().changeRoom(this.nextRoom);
    }

    public void exitRoom(String direction) {
        this.playerLocationHandler.verifyRoomChange(direction);
    }

    public void showInventory() {
        getThePlayer().printInventory();
    }

    public void showEquipment() {
        String target = getTheTarget();
        if (!target.isEmpty()) {
            equipment.equipItem(target);
        } else {
            getThePlayer().printEquipment();
        }
    }

    public void removeEquipment() {
        equipment.unequipItem(getTheTarget());
    }

    public void playerStatus() {
        gameState.getWorld().getPlayer().printStatus();
    }

    public void addToInventory() {
        getThePlayer().putItemInInventory(getTheTarget(), getTheRoom());
    }

    public void removeFromInventory() {
        getThePlayer().removeItemFromInventory(getTheTarget(), getTheRoom());
    }

    public void attack() {
        Room thisRoom = getTheRoom();
        NonPlayer attackTarget = thisRoom.checkRoomForNonPlayer(getTheTarget());
        if (getTheCreatures().isEmpty()) {
            this.combatPrintables.noCreatures();
            return;
        }
        if (attackTarget == null) {
            this.combatPrintables.targetNotFound();
            thisRoom.printCreatures();
            return;
        }
        this.combat.attackNonPlayer(attackTarget, thisRoom, getThePlayer());
    }

    public void showCombatInfo() {
        NonPlayer target = this.gameState.getCurrentCombatTarget();
        if (this.gameState.getCombat()) {
            if (target != null) {
                this.combatPrintables.whichTarget(target.toString());
            }
        } else {
            this.combatPrintables.nonCombat();
        }
    }

    public void roomStatus() {
        getTheRoom().printInfo();
    }

    public void checkThing() {
        Player player = getThePlayer();
        Room thisRoom = getTheRoom();
        String target = getTheTarget();
        this.commandHelper.examine(CommandPrintables.EQUIPMENT_CHECK, player.checkEquipmentForItem(target));
        this.commandHelper.examine(CommandPrintables.INVENTORY_CHECK, player.checkInventoryForItem(target));
        this.commandHelper.examine(CommandPrintables.ROOM_CHECK, thisRoom.checkRoomForNonPlayer(target));
        this.commandHelper.examine(CommandPrintables.ROOM_CHECK, thisRoom.checkRoomForItem(target));
    }

    public void quitGame() {
        this.commandPrintables.goodbye();
        this.gameState.setPlaying(false);
    }

    public void saveGame() {
        entityExporter.writeWorld(gameState.getWorld());
    }
}
