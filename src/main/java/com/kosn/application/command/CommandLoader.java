package com.kosn.application.command;
import static com.google.common.collect.Lists.newArrayList;
import static com.google.common.collect.Sets.newHashSet;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.kosn.application.Input;
import com.kosn.util.printables.CommandPrintables;

public class CommandLoader {

    String previousCommand = "";
    String previousTarget = "";
    Commands commands = Commands.getInstance();
    CommandRunner commandRunner = CommandRunner.getInstance();
    private static final String PREVIOUS_COMMAND = "prev";
    CommandPrintables commandPrintables = CommandPrintables.getInstance();
    // Input input = Input.getInstance();

    Map<String, Runnable> commandMap = new HashMap<String, Runnable>() {
        private static final long serialVersionUID = 1L;

        {
            put("north", () -> commands.exitRoom("north"));
            put("n", () -> commands.exitRoom("north"));
            put("south", () -> commands.exitRoom("south"));
            put("s", () -> commands.exitRoom("south"));
            put("east", () -> commands.exitRoom("east"));
            put("e", () -> commands.exitRoom("east"));
            put("west", () -> commands.exitRoom("west"));
            put("w", () -> commands.exitRoom("west"));
            put("warp", () -> commands.warp());

            put("i", () -> commands.showInventory());
            put("inv", () -> commands.showInventory());
            put("inventory", () -> commands.showInventory());

            put("equip", () -> commands.showEquipment());
            put("equipment", () -> commands.showEquipment());

            put("unequip", () -> commands.removeEquipment());

            put("player", () -> commands.playerStatus());
            put("p", () -> commands.playerStatus());
            put("get", () -> commands.addToInventory());
            put("take", () -> commands.addToInventory());
            put("put", () -> commands.removeFromInventory());
            put("attack", () -> commands.attack());
            put("combat", () -> commands.showCombatInfo());
            put("look", () -> commands.roomStatus());
            put("check", () -> commands.checkThing());
            put("quit", () -> commands.quitGame());
            put("commands", () -> printCommands());
            put("help", () -> printCommands());
            put("use", () -> commands.useItem());
            put("save", () -> commands.saveGame());
        }
    };

    // singleton
    private static CommandLoader instance = null;

    protected CommandLoader() {
    }

    public static CommandLoader getInstance() {
        if (instance == null) {
            instance = new CommandLoader();
        }
        return instance;
    }

    public void processCommand(String command) {
        if (command.equals(PREVIOUS_COMMAND) && !this.previousCommand.isEmpty()) {
            commandRunner.runTheThing(loadPreviousCommandMethod());
            return;
        } else {
            Runnable theCommand = loadCommandMethod(command);
            commandRunner.runTheThing(theCommand);
            return;
        }
    }

    Runnable loadCommandMethod(String command) {
        this.previousCommand = command;
        this.previousTarget = Input.getTarget();
        return this.commandMap.get(command);
    }

    Runnable loadPreviousCommandMethod() {
        Input.setTarget(this.previousTarget);
        return this.commandMap.get(this.previousCommand);
    }

    protected void printCommands() {
        List<String> availableCommands = getCommandsAsSortedList();
        this.commandPrintables.commands(availableCommands);
    }

    List<String> getCommandsAsSortedList() {
        Set<String> commandSet = newHashSet();
        commandSet = this.commandMap.keySet();
        List<String> availableCommands = newArrayList(commandSet);
        Collections.sort(availableCommands);
        return availableCommands;
    }
}
