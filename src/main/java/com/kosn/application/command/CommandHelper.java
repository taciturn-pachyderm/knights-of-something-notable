package com.kosn.application.command;

import static com.google.common.collect.Maps.newHashMap;

import java.util.Map;

import com.kosn.application.GameState;
import com.kosn.entity.Examinable;
import com.kosn.entity.Item;
import com.kosn.entity.Room;
import com.kosn.util.FlowManagement;
import com.kosn.util.printables.CommandPrintables;

public class CommandHelper {
    CommandPrintables commandPrintables = CommandPrintables.getInstance();
    GameState gameState = GameState.getInstance();

    // singleton
    private static CommandHelper instance = null;

    protected CommandHelper() {
    }

    public static CommandHelper getInstance() {
        if (instance == null) {
            instance = new CommandHelper();
        }
        return instance;
    }

    protected Map<String, Item> getUsableItems(Room thisRoom, String target) {
        Map<String, Item> itemsFound = newHashMap();
        Item targetItem = thisRoom.checkRoomForItem(target);
        if (targetItem != null) {
            itemsFound.put("room", targetItem);
        }
        targetItem = gameState.getWorld().getPlayer().checkInventoryForItem(target);
        if (targetItem != null) {
            itemsFound.put("inventory", targetItem);
        }
        return itemsFound;
    }

    void examine(String phrase, Examinable target) {
        if (target == null) {
            return;
        }
        this.commandPrintables.checkTarget(phrase, target);
        FlowManagement.printSomeWaitingDots();
        target.printInfo();
    }
}
