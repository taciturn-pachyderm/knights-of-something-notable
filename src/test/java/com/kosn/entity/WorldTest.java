package com.kosn.entity;

import java.util.Map;
import java.util.Map.Entry;

import com.kosn.application.Direction;
import com.kosn.application.WorldBuilder;
import com.kosn.entity.Room;

public class WorldTest {

    // note: change toString of Room to print only name
    //	@Test
    void verify_room_connections() {
        WorldBuilder worldBuilder = WorldBuilder.getInstance();
        worldBuilder.buildNewWorld("");
        Map<String, Room> worldRooms = worldBuilder.getRooms();
        System.out.println(worldRooms.values());
        int count = 0;
        for (Entry<String, Room> r0 : worldBuilder.getRooms().entrySet()) {
            count++;
            if (count > 2) {
                break;
            }
            System.out.println("Exits for " + r0.getValue().getName());
            for (Entry<Direction, String> r1 : r0.getValue().getExits().entrySet()) {
                System.out.println("---> " + r1.getValue() + " ---> (" + worldRooms.get(r1.getValue()).getExits() + ")");
                for (Entry<Direction, String> r2: worldRooms.get(r1.getValue()).getExits().entrySet()) {
                    System.out.println("------> " + r2.getValue() + " ---> (" + worldRooms.get(r2.getValue()).getExits() + ")");
                    for (Entry<Direction, String> r3: worldRooms.get(r2.getValue()).getExits().entrySet()) {
                        System.out.println("---------> " + r3.getValue() + " ---> (" + worldRooms.get(r3.getValue()).getExits() + ")");
                        for (Entry<Direction, String> r4: worldRooms.get(r3.getValue()).getExits().entrySet()) {
                            System.out.println("------------> " + r4.getValue() + " ---> (" + worldRooms.get(r4.getValue()).getExits() + ")");
                            for (Entry<Direction, String> r5: worldRooms.get(r4.getValue()).getExits().entrySet()) {
                                System.out.println("---------------> " + r5.getValue() + " ---> (" + worldRooms.get(r5.getValue()).getExits() + ")");
                            }
                        }
                    }
                }
            }
        }
    }
}
