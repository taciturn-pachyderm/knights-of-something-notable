package com.kosn.entity;

import com.kosn.application.WorldBuilder;
import com.kosn.db.EntityFileManager;

public class TestWorldGenerator {

    EntityFileManager entityFileManager = EntityFileManager.getInstance();
    WorldBuilder worldBuilder = WorldBuilder.getInstance();
    private static final String A_TEST_PLAYER = "a-test-player";

    public World buildATestWorldSave() {
        worldBuilder.loadEntityPools();
        World world = worldBuilder.buildNewWorld(A_TEST_PLAYER);
        entityFileManager.writeWorld(world);
        return world;
    }

    public void cleanUpTheTestWorldSave() {
        entityFileManager.deletePlayerDir(A_TEST_PLAYER);
    }

    public void loadTheTestWorldSave() {
        entityFileManager.loadWorld(A_TEST_PLAYER);
    }
}
