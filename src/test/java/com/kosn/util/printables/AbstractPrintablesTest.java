package com.kosn.util.printables;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import org.testng.annotations.BeforeMethod;

public abstract class AbstractPrintablesTest {

	protected ByteArrayOutputStream outContent;

	@BeforeMethod
	public void setUpStreams() {
		outContent = new ByteArrayOutputStream();
	    System.setOut(new PrintStream(outContent));
	}
	
}
