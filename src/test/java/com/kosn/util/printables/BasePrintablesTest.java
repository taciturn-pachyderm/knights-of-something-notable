package com.kosn.util.printables;

import static com.google.common.collect.Lists.newArrayList;
import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;

import org.testng.annotations.Test;

public class BasePrintablesTest extends AbstractPrintablesTest {

	@Test
	public void stringWithNoParamsGetsPrinted() {
		String expected = "SomeString\n";
		String toPrint = "SomeString";
		BasePrintables.print(toPrint); 
		assertThat(outContent.toString()).isEqualTo(expected);
	}

	@Test
	public void stringWithOneParamGetsPrinted() {
		String expected = "SomeString value\n";
		String toPrint = "SomeString %s";
		BasePrintables.print(toPrint, "value"); 
		assertThat(outContent.toString()).isEqualTo(expected);
	}

	@Test
	public void stringWithMultipleParamsGetsPrinted() {
		String expected = "SomeString value1 value2 value3\n";
		String toPrint = "SomeString %s %s %s";
		BasePrintables.print(toPrint, "value1", "value2", "value3"); 
		assertThat(outContent.toString()).isEqualTo(expected);
	}

	@Test
	public void stringListOfStringsAsParamGetsPrinted() {
		String expected = "SomeString [value1, value2, value3]\n";
		List<String> listToPrint = newArrayList();
		listToPrint.add("value1");
		listToPrint.add("value2");
		listToPrint.add("value3");
		String toPrint = "SomeString %s";
		BasePrintables.print(toPrint, listToPrint); 
		assertThat(outContent.toString()).isEqualTo(expected);
	}

	@Test
	public void stringWithIntAsParamGetsPrinted() {
		String expected = "SomeString 1\n";
		String toPrint = "SomeString %s";
		BasePrintables.print(toPrint, 1); 
		assertThat(outContent.toString()).isEqualTo(expected);
	}

}