package com.kosn.util.printables;

import static org.assertj.core.api.Assertions.assertThat;

import org.testng.annotations.Test;

public class EntityPrintablesTest extends AbstractPrintablesTest {

	BasePrintables entityPrintables = EntityPrintables.getInstance();

	@Test(description="Test combat round printing" )
	public void combatRoundProcessing_ShouldPrint() {
		String playerName = "someguy";
		String nonPlayerName = "someNonPlayer";
		int playerAttack = 1; 
		String expected = String.format("%s hits %s for %d point(s) of damage!\n", playerName, nonPlayerName, playerAttack);
		EntityPrintables.attackRound(playerName, nonPlayerName, playerAttack);
		assertThat(outContent.toString()).isEqualTo(expected);
	}	
}
