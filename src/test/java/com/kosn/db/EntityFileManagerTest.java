package com.kosn.db;

import static org.assertj.core.api.Assertions.assertThat;

import java.io.IOException;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.kosn.application.WorldBuilder;
import com.kosn.entity.TestWorldGenerator;
import com.kosn.entity.World;

public class EntityFileManagerTest {

    EntityFileManager entityExporter = EntityFileManager.getInstance();
    WorldBuilder worldBuilder = WorldBuilder.getInstance();
    EntityFileManager entityFileManager = EntityFileManager.getInstance();
    EntityFactory entityFactory = EntityFactory.getInstance();
    private static final String A_TEST_PLAYER = "a-test-player";

    @Test
    void worldSaveSavesWorld() throws JsonParseException, JsonMappingException, IOException {
        TestWorldGenerator twg = new TestWorldGenerator();
        World world = twg.buildATestWorldSave();
        World loadedWorld = entityFileManager.loadWorld(A_TEST_PLAYER);
        assertThat(loadedWorld).isEqualToComparingOnlyGivenFields(world,
                "rooms", "currentRoom", "defaultRoom",
                "player.level", "player.exp", "player.description", "player.name",
                "player.money", "player.hitPoints", "player.expToNextLevel"
                );
    }

    @AfterMethod
    void cleanup() {
        entityExporter.deletePlayerDir(A_TEST_PLAYER);
    }
}
