package com.kosn.application.command;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import org.testng.annotations.Test;

import com.kosn.application.PlayerLocationHandler;

public class CommandsTest {

	Commands cp = Commands.getInstance();

	@Test
	void exitRoomShouldCallPlayerHandler() {
		this.cp.playerLocationHandler = mock(PlayerLocationHandler.class);
		this.cp.exitRoom("east");
		verify(this.cp.playerLocationHandler).verifyRoomChange("east");
	}

}
