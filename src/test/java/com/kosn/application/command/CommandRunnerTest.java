package com.kosn.application.command;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import org.testng.annotations.Test;

import com.kosn.util.printables.CommandPrintables;

public class CommandRunnerTest {

    CommandRunner commandRunner = CommandRunner.getInstance();

    @Test
    void processCommandShouldPrintInvalidCommand_withInvalidCommand() {
        this.commandRunner.commandPrintables = mock(CommandPrintables.class);
        this.commandRunner.runTheThing(null);
        verify(this.commandRunner.commandPrintables).noCommand();
    }

}
