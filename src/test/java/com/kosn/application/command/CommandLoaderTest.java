package com.kosn.application.command;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import org.testng.annotations.Test;

import com.kosn.application.Input;
import com.kosn.util.printables.CommandPrintables;

public class CommandLoaderTest {

	CommandLoader commandLoader = CommandLoader.getInstance();

	@Test
	void processCommandShouldCallCommandRunner_withValidCommand() {
		this.commandLoader.commandRunner = mock(CommandRunner.class);
		this.commandLoader.processCommand("north");
		verify(this.commandLoader.commandRunner).runTheThing(this.commandLoader.commandMap.get("north"));
	}

	@Test
	void processPreviousCommandShouldCallCommandRunner_withPreviousCommand() {
		this.commandLoader.previousCommand = "north";
		this.commandLoader.commandRunner = mock(CommandRunner.class);
		this.commandLoader.processCommand("prev");
		verify(this.commandLoader.commandRunner).runTheThing(this.commandLoader.commandMap.get("north"));
	}

	@Test
	void loadCommandMethod_shouldLoadMethod() {
		// Input input = mock(Input.class);
		// when((input).getTarget()).thenReturn("");
		Input.setTarget("");
		Runnable expected = commandLoader.commandMap.get("north");
		Runnable actual = commandLoader.loadCommandMethod("north");
		assert actual.equals(expected);
		assert commandLoader.previousCommand.equals("north");
		assert commandLoader.previousTarget.equals("");
	}

	@Test
	void loadPreviousCommandMethodshouldLoadPreviousMethod_whenPreviousCommandSet() {
		// Input input = Input.getInstance();
		String command = "north";
		commandLoader.previousTarget = "";
		commandLoader.previousCommand = command;
		Runnable expected = commandLoader.commandMap.get(command);
		Runnable actual = commandLoader.loadPreviousCommandMethod();
		assert actual.equals(expected);
		assert Input.getTarget().equals("");
	}

	@Test
	void loadPreviousCommandMethodShouldCallRunCommand_whenPreviousCommandNotSet() {
		commandLoader.previousCommand = "";
		CommandRunner mock = mock(CommandRunner.class);
		this.commandLoader.commandRunner = mock;
		commandLoader.processCommand("prev");
		verify(this.commandLoader.commandRunner).runTheThing(null);
	}

	@Test
	void printCommandsShouldCallPrintables_command() {
		CommandPrintables mock = mock(CommandPrintables.class);
		this.commandLoader.commandPrintables = mock;
		this.commandLoader.printCommands();
		verify(this.commandLoader.commandPrintables).commands(this.commandLoader.getCommandsAsSortedList());
	}
}
