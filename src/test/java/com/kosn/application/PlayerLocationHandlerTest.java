package com.kosn.application;

import static com.google.common.collect.Maps.newHashMap;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Map;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.kosn.application.combat.Combat;
import com.kosn.entity.NonPlayer;
import com.kosn.entity.Player;
import com.kosn.entity.PlayerDefaults;
import com.kosn.entity.Room;
import com.kosn.entity.World;
import com.kosn.util.Executor;
import com.kosn.util.Randomizer;
import com.kosn.util.printables.CombatPrintables;

public class PlayerLocationHandlerTest {

    PlayerLocationHandler plh = PlayerLocationHandler.getInstance();
    Room r1 = new Room();
    Room r2 = new Room();
    World world;

    @BeforeMethod
    void setup() {
        this.r1.setName("room1");
        this.r2.setName("room2");

        Map<Direction, String> room1exits = newHashMap();
        room1exits.put(Direction.east, r2.getName());
        this.r1.setExits(room1exits);

        Map<Direction, String> room2exits = newHashMap();
        room2exits.put(Direction.west, r1.getName());
        this.r2.setExits(room2exits);

        Map<String, Room> rooms = newHashMap();
        rooms.put(r1.getName(), r1);
        rooms.put(r2.getName(), r2);

        WorldBuilder.getInstance().loadEntityPools();
        this.world = WorldBuilder.getInstance().buildNewWorld("");
        this.world.setRooms(rooms);
        this.world.setCurrentRoom(r1);
        GameState.getInstance().setWorld(this.world);
        NonPlayer combatTarget = mock(NonPlayer.class);
        GameState.getInstance().setCurrentCombatTarget(combatTarget);

        this.plh.combatPrintables = mock(CombatPrintables.class);
    }

    @Test
    void getNextRoomShouldReturnRoom_withValidDirection() {
        Room room = this.plh.getNextRoom("east");
        assertThat(room).isEqualTo(r2);
    }

    @Test
    void verifyProcessNonPlayerAttackIsCalled() {
        this.plh.combat = mock(Combat.class);
        NonPlayer combatTarget = mock(NonPlayer.class);
        GameState.getInstance().setCurrentCombatTarget(combatTarget);
        this.plh.unableToEscape();
        verify(this.plh.combat).processNonPlayerAttack(world.getPlayer(), combatTarget);
    }

    @Test
    void escapeShouldCallPrintRunawayTaunt() {
        this.plh.combatPrintables = mock(CombatPrintables.class);
        this.plh.escape();
        verify(this.plh.combatPrintables).runAwayTaunt();
    }

    @Test
    void escapeShouldSetEscapeStatus() {
        this.plh.escape();
        assertThat(plh.escapeStatus).isEqualTo("success");
    }

    @Test
    void unableToEscapeShouldCallCannotEscapeMessage() {
        this.plh.combatPrintables = mock(CombatPrintables.class);
        this.plh.combat = mock(Combat.class);
        NonPlayer combatTarget = mock(NonPlayer.class);
        GameState.getInstance().setCurrentCombatTarget(combatTarget);
        this.plh.unableToEscape();
        verify(this.plh.combatPrintables).cannotEscape();
    }

    @Test
    void executeEscapeRoll_callsExecutor() {
        this.plh.executor = mock(Executor.class);
        this.plh.randomizer = mock(Randomizer.class);
        when(this.plh.randomizer.getRandomBetweenZeroAndRange(6)).thenReturn(Integer.valueOf(3));
        Runnable runnable = this.plh.rollResults.get(3);
        this.plh.executeEscapeRoll();
        verify(this.plh.executor).execute(runnable);
    }

    @Test
    void escapeWithMoneyLoss_adjustsPlayerMoney() {
        Player player = new Player(new PlayerDefaults());
        int startingMoney = 10;
        player.setMoney(startingMoney);
        world.setPlayer(player);
        int moneyLost = 1;
        this.plh.escapeWithMoneyLoss();
        assertThat(world.getPlayer().getMoney()).isEqualTo(startingMoney - moneyLost);
    }

    @Test
    void escapeWithDamage_callsKillPlayer() {
        this.plh.combat = mock(Combat.class);
        NonPlayer combatTarget = mock(NonPlayer.class);
        GameState.getInstance().setCurrentCombatTarget(combatTarget);
        when(this.plh.combat.processNonPlayerAttack(world.getPlayer(), combatTarget)).thenReturn("respawned");
        this.plh.escapeWithDamage();
        // proves that we got here without failing
        assertThat(true);
    }
}
