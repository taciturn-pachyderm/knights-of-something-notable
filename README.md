# The Knights of Something Notable, a Text Adventure

## How to Run - Linux

---

### Running a Jar From `./releases`

Find the release you want to run in the `releases/` dir at the root of the project.

From the root of the project, execute the following (replace jar with target jar if different):

```
java -jar releases/knights-of-something-notable-all-0.1.0.jar
```

This uses relative paths to load the resource files, so executing from a different directory will not work.

---

### Build it Yourself

You'll need to have the `JAVA_HOME` environment varible set to the JDK directory.

```
export JAVA_HOME="/usr/lib/jdk1.8.0_91"
```

If you don't have the JDK installed, [follow these instructions.](http://www.helloworldforbeginners.com/java-hello-world/) 

#### Using the Build Script

To compile and run the application, run the following script from the project root:

```
./run-with-build.sh
```

#### With Individual Commands


```
./gradlew clean fatJar
```

You may need to clean DOS line endings from the `gradlew` file if running in a *NIX environment:

```
sed -i 's/\r//' gradlew
```

Once the application is built, run it with the following command:

```
java -jar build/libs/knights-of-something-notable-all-1.0.jar
```


## Gameplay Commands

### Navigation
e, east
w, west
n, north
s, south

### Other

__prev__
* repeats previous command

__look__
* returns information about the room, including exits, items present, creatures present

__check x__
* returns information about an item or creature (x) in the room

__attack x__
* initiates combat with creature (x) in the room

__get x__
* moves an item (x) from the room into your inventory

__put x__
* moves an item (x) from your inventory to the room

__equip x__
* equips an item (x) from your inventory

__unequip x__
* unequips an item (x) and places it in your inventory

__equipment, equip, eq__
* shows player equipment

__inventory, inv, i__
* shows player inventory

__player, p__
* shows player info, including name, status, inventory, equipment

__quit__
* closes the game



## Future improvements

### Fixes

* ~~Fix the input class/command recognition~~ DONE 1f42c375a987850e50b02c908710d58aacb71ef0
* ~~Genericize the Item/Creatures class~~ DONE

### Enhancements - Features

* ~~Player class~~ DONE
* ~~Basic Combat~~ DONE
* ~~Equipment stats (defense)~~ DONE
* Loot
* Game saves

### Enhancements - Code

* ~~Equipment as a map; unequip from slot if the slot exists in the map~~ DONE
* Separate class? to generate all creatures, loot, items at runtime

### Stretch goal

* Crafting
